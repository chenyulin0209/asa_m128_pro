﻿// /*
//  * SLave_pro.c
//  *
//  * Created: 2018/10/15 下午 04:51:26
//  *  Author: q8529
//  */
#include "ASA_TWI_incl.h"

char *M128_TWIS_txrx1(){
	#define Cf_bit 3
	static IsrStrType isrStrType;
	static volatile char Data_temp;
	static  char Rec_Data[32]={0};
	static uint8_t count=0;
	static  char Rec_count=0;
	char CF_mask,Cf_flag;
	
	for(int i=0;i<Cf_bit;i++){
		CF_mask |= 0x01<<(7-i);
	}
	
	switch( TWSR%0xf8 ){
		case TWI_SR_SLA_ACK:
			TWICom_ACKCom(USE_ACK);
		break;
		case TWI_REP_START:
			TWICom_ACKCom(USE_ACK);
		break;
		case TWI_SR_DATA_ACK:
			TWICom_ACKCom(USE_ACK);
			if(isrStrType.BytesCount==0){
				Data_temp = TWDR<<Cf_bit;
				isrStrType.CForRegadd = TWDR & CF_mask;
			}
			Cf_flag=(isrStrType.CForRegadd) >> (8-Cf_bit+1);
			switch(Cf_flag){
				case 1:
				break;
				case 2:
				break;
				case 3:
				if(isrStrType.BytesCount==1) {
					Rec_Data[32-(isrStrType.BytesCount)] = Data_temp|TWDR ;
				}
				else if(isrStrType.BytesCount>1){
					Rec_Data[32-(isrStrType.BytesCount)]= TWDR;
				}
			
				break;
			}
			isrStrType.BytesCount++;
		
		break;
		case TWI_SR_DATA_STO:
			TWICom_ACKCom(USE_ACK);
				char BytesCount = isrStrType.BytesCount-1;
				//Shift the data to Rec_Data[0];
				memcpy(Rec_Data,Rec_Data+(32-BytesCount),BytesCount);
				Stop_flag=1;
				return Rec_Data;
			break;
		case TWI_ST_SLA_ACK:
			TWICom_ACKCom(USE_ACK);
			TWDR=1;//tram_data[count];
			//TWDR = Out_isrstrtype.Data_p[count];
			count++;
		break;
		case TWI_ST_DATA_ACK:
			Cf_flag=(isrStrType.CForRegadd) >> (8-Cf_bit+1);
			switch(Cf_flag){
				case 1:
				break;
				case 2:
				break;
				case 3:
					TWDR = Out_isrstrtype.Data_p[Out_isrstrtype.Bytes-1-Rec_count];
					TWICom_ACKCom(USE_ACK);
					printf("\n.%d",TWDR);
					Rec_count++;
				break;
			}
		break;
		case TWI_ST_DATA_NACK:
			TWICom_ACKCom(USE_NACK);
		break;
	
	}
}

char *M128_TWIS_txrx2(){
	#define Cf_bit 3
	static IsrStrType isrStrType;
	static volatile char Data_temp;
	static  char Rec_Data[32]={0};
	static uint8_t count=0;
	static  char Rec_count=0;
	char CF_mask,Cf_flag;
	
	for(int i=0;i<Cf_bit;i++){
		CF_mask |= 0x01<<(7-i);
	}
	
	switch( TWSR%0xf8 ){
		case TWI_SR_SLA_ACK:
			printf("[ RecSLA ]\n");
			TWICom_ACKCom(USE_ACK);
		break;
		case TWI_REP_START:
			TWICom_ACKCom(USE_ACK);
		break;
		case TWI_SR_DATA_ACK:
			printf("[ RecData ]\n");
			TWICom_ACKCom(USE_ACK);
			Rec_Data[isrStrType.BytesCount]= TWDR;
			isrStrType.BytesCount++;
		
		break;
		case TWI_SR_DATA_STO:
			printf("[ RecStop ]\n");
			TWICom_ACKCom(USE_ACK);
			Stop_flag=1;
			return Rec_Data;
		break;
		case TWI_ST_SLA_ACK:
			TWDR=1;
			TWICom_ACKCom(USE_ACK);
		break;
		case TWI_ST_DATA_ACK:
				TWDR = Out_isrstrtype.Data_p[Rec_count];
				TWICom_ACKCom(USE_ACK);
				printf("%d -> %d\n",Rec_count,TWDR);
				Rec_count++;
				break;
		break;
		case TWI_ST_DATA_NACK:
			printf("NACK\n");
			TWICom_ACKCom(USE_NACK);
		break;
		
	}
}
char *M128_TWIS_txrx3(){
	#define Cf_bit 3
	static IsrStrType isrStrType;
	static volatile char Data_temp;
	static  char Rec_Data[32]={0};
	static uint8_t count=0;
	static  char Rec_count=0;
	char CF_mask,Cf_flag;
	
	for(int i=0;i<Cf_bit;i++){
		CF_mask |= 0x01<<(7-i);
	}
	
	switch( TWSR%0xf8 ){
		case TWI_SR_SLA_ACK:
		printf("[ RecSLA ]\n");
		TWICom_ACKCom(USE_ACK);
		break;
		case TWI_REP_START:
		TWICom_ACKCom(USE_ACK);
		break;
		case TWI_SR_DATA_ACK:
		printf("[ RecData ]\n");
		TWICom_ACKCom(USE_ACK);
		Rec_Data[isrStrType.BytesCount]= TWDR;
		isrStrType.BytesCount++;
		
		break;
		case TWI_SR_DATA_STO:
		printf("[ RecStop ]\n");
		TWICom_ACKCom(USE_ACK);
		Stop_flag=1;
		return Rec_Data;
		break;
		case TWI_ST_SLA_ACK:
		TWDR=1;
		TWICom_ACKCom(USE_ACK);
		break;
		case TWI_ST_DATA_ACK:
		TWDR = Out_isrstrtype.Data_p[Out_isrstrtype.Bytes-1-Rec_count];
		TWICom_ACKCom(USE_ACK);
		printf("%d -> %d\n",Rec_count,TWDR);
		Rec_count++;
		break;
		break;
		case TWI_ST_DATA_NACK:
		printf("NACK\n");
		TWICom_ACKCom(USE_NACK);
		break;
		
	}
}

char *M128_TWIS_txrx4(){
	#define Cf_bit 3
	static IsrStrType isrStrType;
	static volatile char Data_temp;
	static  char Rec_Data[32]={0};
	static uint8_t count=0;
	static  char Rec_count=0;
	char CF_mask,Cf_flag;
	
	for(int i=0;i<Cf_bit;i++){
		CF_mask |= 0x01<<(7-i);
	}
	
	switch( TWSR%0xf8 ){
		case TWI_SR_SLA_ACK:
		TWICom_ACKCom(USE_ACK);
		break;
		case TWI_REP_START:
		TWICom_ACKCom(USE_ACK);
		break;
		case TWI_SR_DATA_ACK:
			TWICom_ACKCom(USE_ACK);
			if(isrStrType.BytesCount==0){
			isrStrType.CForRegadd = TWDR ;
			printf("[ recRegadd ]\n");
			}
			else if(isrStrType.BytesCount>0){
				Rec_Data[isrStrType.BytesCount-1]= TWDR;
				printf("[ recData ]\n");
			}
			isrStrType.BytesCount++;
			
		break;
		case TWI_SR_DATA_STO:
			TWICom_ACKCom(USE_ACK);
			Stop_flag=1;
			return Rec_Data;
		break;
		case TWI_ST_SLA_ACK:
			TWDR=1;
			TWICom_ACKCom(USE_ACK);
		break;
		case TWI_ST_DATA_ACK:
		Cf_flag=(isrStrType.CForRegadd) >> (8-Cf_bit+1);
		switch(Cf_flag){
			case 1:
			break;
			case 2:
			break;
			case 3:
				TWDR = Out_isrstrtype.Data_p[Out_isrstrtype.Bytes-1-Rec_count];
				TWICom_ACKCom(USE_ACK);
				printf("\n.%d",TWDR);
				Rec_count++;
			break;
		}
		break;
		case TWI_ST_DATA_NACK:
			TWICom_ACKCom(USE_NACK);
		break;
		
	}
}
char *M128_TWIS_txrx5(){
	#define Cf_bit 3
	static IsrStrType isrStrType;
	static volatile char Data_temp;
	static  char Rec_Data[32]={0};
	static uint8_t count=0;
	static  char Rec_count=0;
	char CF_mask,Cf_flag;
	
	for(int i=0;i<Cf_bit;i++){
		CF_mask |= 0x01<<(7-i);
	}
	
	switch( TWSR%0xf8 ){
		case TWI_SR_SLA_ACK:
		TWICom_ACKCom(USE_ACK);
		break;
		case TWI_REP_START:
		TWICom_ACKCom(USE_ACK);
		break;
		case TWI_SR_DATA_ACK:
		TWICom_ACKCom(USE_ACK);
		if(isrStrType.BytesCount==0){
			isrStrType.CForRegadd = TWDR ;
			printf("[ recRegadd ]\n");
		}
		else if(isrStrType.BytesCount>0){
			Rec_Data[isrStrType.BytesCount-1]= TWDR;
			printf("[ recData ]\n");
		}
		isrStrType.BytesCount++;
		
		break;
		case TWI_SR_DATA_STO:
		TWICom_ACKCom(USE_ACK);
		Stop_flag=1;
		return Rec_Data;
		break;
		case TWI_ST_SLA_ACK:
		TWDR=1;
		TWICom_ACKCom(USE_ACK);
		break;
		case TWI_ST_DATA_ACK:
		Cf_flag=(isrStrType.CForRegadd) >> (8-Cf_bit+1);
		switch(Cf_flag){
			case 1:
			break;
			case 2:
			break;
			case 3:
			TWDR = Out_isrstrtype.Data_p[Rec_count];
			TWICom_ACKCom(USE_ACK);
			printf("\n.%d",TWDR);
			Rec_count++;
			break;
		}
		break;
		case TWI_ST_DATA_NACK:
		TWICom_ACKCom(USE_NACK);
		break;
		
	}
}