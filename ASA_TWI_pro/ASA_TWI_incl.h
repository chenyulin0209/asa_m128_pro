﻿
/*
 * ASA_TWI_incl.s
 *
 * Created: 2018/10/15 下午 04:56:29
 *  Author: q8529
 */ 
 
#include <avr/io.h>

#ifndef ASA_TWI_CORE_H_
#define ASA_TWI_CORE_H_


#define FOSC 11059200
/*Error codes */
#define SLA_type_error		0xD0

/*Status codes for start and restart condition*/
#define TWI_START			0x08
#define TWI_REP_START		0x10
//--------------------------------------------------------------------------------------------------------------------
/*Status codes for master transmitter mode*/
#define TWI_MT_SLA_ACK		0x18
#define TWI_MT_SLA_NACK		0x20
#define TWI_MT_DATA_ACK		0x28
#define TWI_MT_DATA_NACK	0x30
#define TWI_MT_ARB_LOST		0x38
//--------------------------------------------------------------------------------------------------------------------
/*Status codes for master receiver mode*/
#define	TWI_MR_SLA_ACK		0x40
#define TWI_MR_SLA_NACK		0x48
#define TWI_MR_DATA_ACK		0x50
#define TWI_MR_DATA_NACK	0x58
#define	TWI_MR_ARB_LOST		0x38
//--------------------------------------------------------------------------------------------------------------------
#define TWI_STATUS	((TWSR)&(0xF8))
//--------------------------------------------------------------------------------------------------------------------
/*Status codes for Slave transmitter mode*/
#define TWI_ST_SLA_ACK			0xA8
#define TWI_ST_SLA_Arb_lose_ACK	0xB0
#define TWI_ST_DATA_ACK			0xB8
#define TWI_ST_DATA_NACK		0xC0
#define TWI_ST_Data_last		0xC8
//--------------------------------------------------------------------------------------------------------------------
/*Status codes for Slave receiver mode*/
#define	TWI_SR_SLA_ACK				0x60
#define TWI_SR_SLA_Arb_lose_ACK		0x68
#define TWI_SR_SLA_Gen_ACK			0x70
#define TWI_SR_SLA_ArbLos_Gen_ACK	0x78
#define TWI_SR_DATA_ACK				0x80
#define TWI_SR_DATA_NACK			0x88
#define TWI_SR_DATA_Gen_ACK			0x90
#define TWI_SR_DATA_Gen_NACK		0x98
#define TWI_SR_DATA_STO				0xA0

#define USE_ACK 0x01
#define USE_NACK 0x00

#define Data_Write	0x00
#define Data_Read	0x01

// char M128_TWI_set(char LSByte, char Mask,  char shift,char Data);
// char M128_TWI_put(char LSByte, char Bytes, char *Data_p);
// char M128_TWI_get(char LSByte, char Bytes, char *Data_p);
// char M128_TWI_fpt( char LSByte, char Mask,  char shift, char Data);
// char M128_TWI_fgt( char LSByte, char Mask,  char shift,  char *Data_p);
// char M128_TWIM_trm(char OneReg, char TWIID, char RegAdd, char Bytes, void *Data_p);
// char M128_TWIM_rec(char OneReg, char TWIID, char RegAdd, char Bytes, void *Data_p);
// char M128_TWIM_ftm(char SLA,char OneReg, char RegAdd, char Mask, char shift, void *Data_p);
// char M128_TWIM_frc(char SLA,char OneReg, char RegAdd, char Mask, char shift, char *Data_p);

// char M128_TWI_RESTART = 0;
// char M128_TWI_STOP = 0;
// // char Status=0;
// char TWI_Rec_Data[100];


volatile char *rec_data;
volatile char Stop_flag;

char M128_TWIM_trm( char Mode, char SLA_W, char RegAdd, char Bytes, char *Data_p);
char M128_TWIM_rec( char Mode, char SLA_R, char RegAdd, char Bytes, char *Data_p);
char* M128_TWIS_txrx1();
char* M128_TWIS_txrx2();
char* M128_TWIS_txrx3();
char* M128_TWIS_txrx4();
char* M128_TWIS_txrx5();
char TWI_Reg_tram(char reg,char time);
char TWI_Reg_rec(char *data,char time);
void error_state(char state);
void TWICom_ACKCom(char ack);
void TWICom_Stop(char _stop);
void TWICom_ReStart(char _start);


typedef struct {
	unsigned char SLA;
	unsigned char Regadd;
	unsigned char CForRegadd;
	unsigned char BytesCount;
	unsigned char Bytes;
	unsigned char Cycle;
	unsigned char Phase;
	unsigned char *Data_p;
} IsrStrType;

IsrStrType Out_isrstrtype ;
#endif /* ASA_TWI_CORE_H_ */