﻿// /*
//  * Master_pro.c
//  *
//  * Created: 2018/10/15 下午 04:50:45
//  *  Author: q8529
//  */
#include "ASA_TWI_incl.h"
#include <avr/delay.h>



char M128_TWIM_trm( char Mode, char SLA, char RegAdd, char Bytes, char *Data_p){
	
	#define Cf_bit 3
	char state=0;
	char CF_mask=0,LSB_mask=0,CF_DATA=0;
	static char count=0;
	switch (Mode)
	{
		case 1:
		//Sent SLA+W to Slave
		state =TWI_Reg_tram(SLA,0);
		//Count the times of Tram_data length
		for(int i=Bytes;0<=i;i--){
			if(count==0){
				// Sent CF+HSB
				CF_DATA = RegAdd | (Data_p[Bytes-1]>>Cf_bit);
				state = TWI_Reg_tram(CF_DATA,0);
			}
			// Sent LSB
			else if(count==1){
				//--------------------------------------------------------------------------------------------------------------------
				//				CF_mask
				for(int i=0;i<Cf_bit;i++){
					LSB_mask |= 0x01<<(i);
				}
				//--------------------------------------------------------------------------------------------------------------------
				char LSB_DATA = Data_p[Bytes-1] & LSB_mask;
				state=TWI_Reg_tram(LSB_DATA,0);
			}
			// Sent Next bytes(2) data
			else if(count>=2){
				state=TWI_Reg_tram(Data_p[i],0);
				_delay_ms(50);
			}
			count++;
		}
		break;
		case 2:
			state =TWI_Reg_tram(SLA,0);
			printf("[Tram SLA Done]\n");
			//Tram Data Low bit first
			for(int i=0;i<Bytes;i++){
				state=TWI_Reg_tram(Data_p[i],0);
				printf("[Tram %d Data]\n",i);
				_delay_ms(50);
			}
		break;
		case 3:
			state =TWI_Reg_tram(SLA,0);
			printf("[Tram SLA Done]\n");
			//Tram Data ( High bit first )
			for(int i=Bytes-1;0<=i;i--){
				state=TWI_Reg_tram(Data_p[i],0);
				printf("[Tram Data %d]\n",Data_p[i]);
				_delay_ms(50);
			}
		break;
		case 4:
			//Sent SLA+W to Slave
			SLA &= ~0x01;
			state =TWI_Reg_tram(SLA,0);
			printf("[Tram SLA Done]\n");
			
			state = TWI_Reg_tram(RegAdd,0);
			printf("[Tram Regadd Done]\n");
			
			//Tram Data Low bit first
			for(int i=0;i<Bytes;i++){
				state=TWI_Reg_tram(Data_p[i],0);
				_delay_ms(50);
			}
		break;
		case 5:
		//Sent SLA+W to Slave
		SLA &= ~0x01;
		state =TWI_Reg_tram(SLA,0);
		printf("[Tram SLA Done]\n");
		
		state = TWI_Reg_tram(RegAdd,0);
		printf("[Tram Regadd Done]\n");
		
		//Tram Data High bit first
		for(int i=Bytes-1;0<=i;i--){
			state=TWI_Reg_tram(Data_p[i],0);
			_delay_ms(50);
		}
		break;
		
	}
	TWICom_Stop(1);
}


char M128_TWIM_rec( char Mode, char SLA, char RegAdd, char Bytes, char *Data_p){
	
	char state=0;
	char temp_RecData[Bytes];
	char garbage;
	
	printf("[Rec Start]\n");
	switch(Mode){
		case 1:
			//Tram SLA+W
			SLA &=~0x01;
			state =TWI_Reg_tram(SLA,0);
			printf("[tram SLA_1 Done]\n");
			state = TWI_Reg_tram(RegAdd,0);
			printf("[tram reg Done]\n");
			//Transmit Stop
		
			TWICom_ReStart(1);
			
			printf("[2 Start ]\n");
			printf("%x\n", TWSR%0xf8);
			//Tram SLA+R
			SLA = SLA | (0x01);
			state =TWI_Reg_tram(SLA,0);
			printf("[tram SLA_2 Done]\n");
			
			for(int i=1;i<=Bytes+2;i++){
				if(i==Bytes+2){
					TWCR = ( 1<<TWINT ) | (1<<TWSTO);
					garbage=TWDR;
				}
				else if(i==1){
					TWI_Reg_rec(&garbage,1);
				}
				else if(i>1){
					TWI_Reg_rec(&temp_RecData[i-2],1);
				}
			}
			for(int i=0;i<Bytes;i++){
				printf("%d\t",temp_RecData[i]);
			}
			printf("\n-----------\n");
			//<<<<<<< H->L ->-> L->H
			for(int i=0;i<Bytes;i++){
				Data_p[i]=temp_RecData[Bytes-i-1];
				printf("%d\t",Data_p[i]);
			}
			//>>>>>>>
		break;
		case 2:
			SLA |= 0x01;
			//Tram SLA+R 
			state =TWI_Reg_tram(SLA,0);
			for(int i=0;i<Bytes+1;i++){
				if(i==0) TWI_Reg_rec(&garbage,1);
				else{
					TWI_Reg_rec(&Data_p[i-1],1);
					printf("Data : %d\n",Data_p[i-1]);
				}
			}
			TWICom_ACKCom(USE_NACK);
			printf("Garbage:%d\n",garbage);
		break;
		case 3:
			SLA |= 0x01;
			//Tram SLA+R
			state =TWI_Reg_tram(SLA,0);
			for(int i=0;i<Bytes+1;i++){
				if(i==0) TWI_Reg_rec(&garbage,1);
				else{
					// Rec SLave Data : High Bit First
					TWI_Reg_rec(&Data_p[i-1],1);
				}
			}
			TWICom_ACKCom(USE_NACK);
			printf("Garbage:%d\n",garbage);
		break;
		case 4:
			//Tram SLA+W
			SLA &=~0x01;
			state =TWI_Reg_tram(SLA,0);
			printf("[tram SLA_1 Done]\n");
			state = TWI_Reg_tram(RegAdd,0);
			printf("[tram reg Done]\n");
			
			TWICom_ReStart(1);
			printf("[2 Start ]\n");
			printf("%x\n", TWSR%0xf8);
			//Tram SLA+R
			SLA = SLA | (0x01);
			state =TWI_Reg_tram(SLA,0);
			printf("[tram SLA_2 Done]\n");
			for(int i=0;i<Bytes+1;i++){
				if(i==0) TWI_Reg_rec(&garbage,1);
				else{
					TWI_Reg_rec(&Data_p[i-1],1);
					printf("Data : %d\n",Data_p[i-1]);
				}
			}
			TWICom_ACKCom(USE_NACK);
			printf("Garbage:%d\n",garbage);
			
		break;
		case 5:
		//Tram SLA+W
		SLA &=~0x01;
		state =TWI_Reg_tram(SLA,0);
		printf("[tram SLA_1 Done]\n");
		state = TWI_Reg_tram(RegAdd,0);
		printf("[tram reg Done]\n");
		
		TWICom_ReStart(1);
		printf("[2 Start ]\n");
		printf("%x\n", TWSR%0xf8);
		//Tram SLA+R
		SLA = SLA | (0x01);
		state =TWI_Reg_tram(SLA,0);
		printf("[tram SLA_2 Done]\n");
		for(int i=0;i<Bytes+1;i++){
			if(i==0) TWI_Reg_rec(&garbage,1);
			else{
				TWI_Reg_rec(&Data_p[i-1],1);
				printf("Data : %d\n",Data_p[i-1]);
			}
		}
		TWICom_ACKCom(USE_NACK);
		printf("Garbage:%d\n",garbage);
		
		break;
	}
}