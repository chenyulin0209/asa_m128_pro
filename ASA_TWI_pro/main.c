/*
 * ASA_TWI_pro.c
 *
 * Created: 2018/10/15 下午 04:48:55
 * Author : q8529
 */ 

#include <avr/io.h>
#include <avr/delay.h>
#include <avr/interrupt.h>
#include "M128_printf.h"
#include "ASA_TWI_incl.h"



volatile int stat;

//#define Master
#define Slave

void ID_set(char ID){
	// Set ASA_ID
	DDRB=0xe0;
	PORTB=(ID<<PB5);
}

#define Arrary_times 10


#ifdef Master
int main(void){
	ASA_M128_set();
	ID_set(1);
	M128_TWI_Master_set();
//  tramsmit data
// 	char data[Arrary_times];
// 	for(int i=0;i<Arrary_times;i++) { data[i]=i+10; printf("%d\t",data[i]);}
// 		
// 	char Mode=5;
// 	char SLA=0b11101110 ;
// 	char regadd=0b11100000;
// 	char bytes=Arrary_times;
// 	M128_TWIM_trm(Mode,SLA,regadd,bytes,data);

//	rec_data
	
	char Mode=4;
	char SLA=0b11101110 ;
	char regadd=0b11000000;
	char bytes=21;
	char data[bytes];
	M128_TWIM_rec(Mode,SLA,regadd,bytes,&data);
	
	for(int i=0;i<bytes;i++){
		printf("%d\t",data[i]);
	}


}
#endif
#ifdef Slave
int tamp=0;
int main(void)
{
	ASA_M128_set();
	ID_set(1);
	//Slave Setting
	M128_TWI_Slave_set(); 
//	Tramsmit Data

	//Setting  arrary length 
	Out_isrstrtype.Bytes=21;
	//Creact Slave Tramsmit Arrary
	char tran_data[Out_isrstrtype.Bytes];
	for(int i=0;i<Out_isrstrtype.Bytes;i++) tran_data[i]=i+10;
	//Copy Arrary to struct
	Out_isrstrtype.Data_p = tran_data;
	//Enable interrup 
	sei();
	while(1);

// Receive Data
// 	sei();
// 	 while(1){
// 		
// 		 if(Stop_flag==1){
// 			 for(int i=0;i<32;i++){
// 				 printf("%d\t",rec_data[i]);
// 			 }
// 			 break;
// 		 }
// 	 }
	 
}
ISR(TWI_vect){
 	//Tramsmit Data
 	 M128_TWIS_txrx4();
	//Receive Data
//	 rec_data = M128_TWIS_txrx5();
}
#endif




//
void TWICom_ACKCom(char ack){
	if(ack == USE_ACK)
	{TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWEA)|(1<<TWIE);}
	if(ack == USE_NACK)
	{TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWIE);}
}
void TWICom_Stop(char _stop){
	if(_stop == 1)
	{TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWSTO);}
}
void TWICom_ReStart(char _start){
	if(_start==1){
	TWCR =(1<<TWSTA)|( 1<<TWINT )|(1<<TWEN);
	while (!(TWCR & (1<<TWINT)));
	}
} 
void TWICom_Start(char _start){
	if(_start==1){
		TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
		while( !(TWCR & (1<<TWINT)) );
	}
}
char TWI_Reg_tram_ReStar(char reg,char TWEA_Flag){
	// Prepare the data and start the TWI
	TWDR = reg;
	//Enable TWI Acknowledge bit
	if(TWEA_Flag==1) TWCR = ( 1<<TWINT ) | (1<<TWEN)|(1<<TWEA)|(1<<TWSTA);
	else TWCR = ( 1<<TWINT ) | (1<<TWEN) | (1<<TWSTA);
	// Blocking here, wait for TWI operation finish and return
	while (!(TWCR & (1<<TWINT)));
	// check the error
	char state = TWSR%0xf8;
	error_state(state);
	return (TWSR%0xf8);
}
char TWI_Reg_tram(char reg,char TWEA_Flag){
	// Prepare the data and start the TWI
	TWDR = reg;
	//Enable TWI Acknowledge bit
	if(TWEA_Flag==1) TWCR = ( 1<<TWINT ) | (1<<TWEN)|(1<<TWEA);
	else TWCR = ( 1<<TWINT ) | (1<<TWEN);
	// Blocking here, wait for TWI operation finish and return
	while (!(TWCR & (1<<TWINT)));
	// check the error
	char state = TWSR%0xf8;
	error_state(state);
	return (TWSR%0xf8);
}
char TWI_Reg_rec(char *data,char TWEA_Flag){
	//Enable TWI Acknowledge bit
	if(TWEA_Flag==1) TWCR = ( 1<<TWINT ) | (1<<TWEN)|(1<<TWEA);
	else TWCR = ( 1<<TWINT ) | (1<<TWEN);
	// Blocking here, wait for TWI operation finish
	while( !(TWCR & (1<<TWINT)) );
	// The data was ready, store the data and return
	*data=TWDR;
	char state = TWSR%0xf8;
	error_state(state);
	return (TWSR%0xf8);
}
void error_state(char state){
	#define conti 0
	#define stop 1
	char st,error_code=0;
	//--------------------------------------------------------------------------------------------------------------------
	/*Status codes for master transmitter mode*/
	if(state==TWI_MT_SLA_NACK) {		error_code=stop;st=1;}
	else if(state==TWI_MT_DATA_NACK){	error_code=stop;st=2;}
	else if(state==TWI_MT_ARB_LOST){	error_code=stop;st=3;}
	//--------------------------------------------------------------------------------------------------------------------
	/*Status codes for master receiver mode*/
	else if(state==TWI_MR_SLA_NACK) {	error_code=stop;st=4;}
	//else if(state==TWI_MR_DATA_NACK){	error_code=stop;st=5;}
	else if(state==TWI_MR_ARB_LOST){	error_code=stop;st=6;}
	//--------------------------------------------------------------------------------------------------------------------
	/*Status codes for slave transmit mode*/
	else if(state==TWI_ST_SLA_Arb_lose_ACK) {	error_code=conti;st=7;}
	else if(state==TWI_ST_DATA_NACK){			error_code=stop;st=8;}
	//--------------------------------------------------------------------------------------------------------------------
	/*Status codes for Slave receiver mode*/
	else if(state==TWI_SR_SLA_Arb_lose_ACK) {	error_code=conti;st=9;}
	else if(state==TWI_SR_SLA_ArbLos_Gen_ACK){	error_code=conti;st=10;}
	else if(state==TWI_SR_DATA_NACK){			error_code=stop;st=11;}
	else if(state==TWI_SR_DATA_Gen_NACK){		error_code=stop;st=12;}
	
	if(error_code==1)printf("Error Code [ %d ]\n",st);
	while (error_code!=conti) ;
}

void M128_TWI_Master_set(){
	// Set TWI speed // _CPU Clock frequency_/16+2*(TWBR)*4^(prescaler bits) ; prescaler bits = 1
	TWBR = 12;
	// enable TWI TWI_Interrupt and sent "start"
	TWCR =(1<<TWSTA)|(1<<TWEN)|(1<<TWINT); //printf("%x\n",TWCR );
	// Wait for Result (while finish operation, Interrupt Flag bit would be set to 1
	while( !(TWCR & (1<<TWINT)) );
	// success: 0x08
	printf("Start handshake_Code: %x\n",TWSR&0xf8);
	printf("[Master Set Done]\n");
	
}
void M128_TWI_Slave_set(){
	TWAR = 0b11101110;
	// Set TWI speed // _CPU Clock frequency_/16+2*(TWBR)*4^(prescaler bits) ; prescaler bits = 1
	TWBR = 12;
	// enable TWI TWI_Interrupt and shack_hand for Master Start signal
	TWCR = (1<<TWEN)|(1<<TWIE)|(1<<TWINT)|(1<<TWEA);
	// Wait for Result (while finish operation, Interrupt Flag bit would be set to 1
	// success: 0x08
	printf("Start handshake_Code: %x\n",TWSR&0xf8);
	
	printf("[Slave Set Done]\n");
	
}